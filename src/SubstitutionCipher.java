public class SubstitutionCipher implements MessageEncoder {
	/**
	 * shift is a private variable which is number of shifts for encoding
	 */
	
	
	private int shift;
	/**
	 * default constructor
	 */
	
	
	public SubstitutionCipher()
	{
		shift=0;
	}
	
	/**
	 * overloaded constructor
	 */
	public SubstitutionCipher(int shift) {
		this.shift = shift;
	}

	@Override

	public String encode(String plainText)
	{
		
		char[] charArray=plainText.toCharArray();
		for(int i=0;i<charArray.length; i++)
		{
			charArray[i]=shifter(charArray[i]);
			
		}
			return new String(charArray);
		
		
	}
	
/**
 * 
 * @param string is the character which is shifted
 * @return shifted character
 */
	public char shifter(char string)
	{
		int i=(int)string;
		if(i>=65&& string <=90)
		{
			i+=shift;
			if(i>90)
				i=i-26;
			return(char)i;
		}
		else if (i >= 97 && i <= 122)	//a-z
		{
			i += shift;
			if (i > 122)
				i = -26 + i;
			return (char)i;
		}
		else	//everything else
			return string;
		
		
	
	}
	/**
	 * 
	 * @return the number of shift
	 */
	public int getShift() {
		return shift;
	}
	public void setShift(int shift) {
		this.shift = shift;
	}



}


