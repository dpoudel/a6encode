/**
 * @author Deepak
 * @propose: This Program is used to Encode a provided String using given number of shifts. I have used 3 shifts.
 * ShuffleCipher class is used to shuffle the provided string and SubstitutionCipher class is used to encode the string.
 * In this Problem we use the concept of Abstract class and Interface.
 */



import java.util.Scanner;

public class MessageEncoderDemo {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		//SubstitutionCipher aCipher = new SubstitutionCipher(3);
		//ShuffleCipher sCipher = new ShuffleCipher(10); 
		//System.out.println(aCipher.shifter('3'));
		//System.out.println(sCipher.shuffler("tanes kanchanawanchai"));
		
	
		
		
		System.out.println("Please Enter the string to Encode");
		Scanner input=new Scanner(System.in);
		String inputString=input.nextLine();
		
	
	

		SubstitutionCipher aCipher = new SubstitutionCipher(3);
		
		System.out.println("Normal Text: " + inputString);
		System.out.println("Encoded Text after "+aCipher.getShift() + " shift is : "  + aCipher.encode(inputString));
		System.out.println("");
		
		
		
		
		ShuffleCipher sCipher=new ShuffleCipher(10);
		//System.out.println(sCipher.shuffler(inputString));
		System.out.println("Shuffled Text is : "+sCipher.shuffler(inputString));

	}

}