
public class ShuffleCipher implements MessageEncoder {
	
	private int n;
	/**
	 * default constructor
	 */
	
	
	public ShuffleCipher()
	{
		n=0;
	}
	/**
	 * Overloaded constructor
	 */
	public ShuffleCipher(int n) {
		this.n = n;
	}
	@Override
	public String encode(String plainText) {
		return null;
	}
/**
 * 
 * @param message is the string provided by user
 * @return the cipher after shuffle
 */
	public String shuffler(String message) {
		String cipher="";
		String str1,str2;
		
		str1=message.substring(0,message.length()/2+message.length()%2);
		str2=message.substring(message.length()/2+message.length()%2,message.length());
		
		
		
		
		
		for(int i=0;i<str2.length();i++)
			cipher+=str1.substring(i,i+1)+str2.substring(i,i+1);
		if(message.length()%2==1)
		{
			cipher+=str1.substring(str1.length()-1,str1.length());
		}
		
		return cipher;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}





}